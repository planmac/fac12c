USING: accessors colors.constants combinators.smart kernel fry
math math.parser models namespaces sequences ui ui.gadgets
ui.gadgets.borders ui.gadgets.buttons ui.gadgets.labels
ui.gadgets.tracks ui.pens.solid ;

FROM: models => change-model ;

IN: hp12c

TUPLE: register < model
    lastx x y z t 
    R0  R1  R2  R3  R4  R5  R6  R7  R8  R9
    R.0 R.1 R.2 R.3 R.4 R.5 R.6 R.7 R.8 R.9
    n i PV PMT FV ;

SYMBOL: reg
"0" register new-model 0 >>x set-global

! Basic stack functions
: set-x ( -- )
    reg get-global dup
    value>> string>number >>x drop ;

: digit ( dig -- )
    reg get-global swap '[ _ append ] change-model set-x ;

: decimal ( -- )
    reg get-global value>> "." swap subseq? [ "." digit ] unless ;

: change-sign ( -- )
    reg get-global value>> "-" head?
    [ [ 1 tail ] change-model ]
    [ [ "-" prepend ] change-model ] if set-x ;

: enter ( -- )
    reg get-global dup x>> >>y drop ;

: swap-xy ( -- )
    reg get-global dup
    [ x>> ] [ y>> ] bi
    -rot >>x swap >>y drop ; 

! create a label connected to the reg model
: <display> ( -- label )
    reg get-global <label-control> { 5 5 } <border>
        { 1 1/2 } >>align
        COLOR: gray <solid> >>boundary ;

! Make buttons
: [^] ( s q -- button )
    '[ drop _ ] <border-button> ;
    
! Button functions
: [+] ( -- ) 
    reg get-global dup
    x>> >>lastx dup
    [ y>> ] [ x>> ] bi + >>x dup
    [ t>> ] [ z>> ] bi -rot >>y swap >>z 0 >>t drop ;

: <but> ( quot -- button )



: <col> ( quot -- track )
    vertical <track> 1 >>fill { 5 5 } >>gap
    swap output>array [ 1 track-add ] each ; inline
    
: <row> ( quot -- track )
    horizontal <track> 1 >>fill { 5 5 } >>gap
    swap output>array [ 1 track-add ] each ; inline            

! Make the button grids

: make-left ( -- ) 
  [
    [ [ "n"   [n]   ]
      [ "i"   [i]   ]
      [ "PV"  [PV]  ]
      [ "PMT" [PMT] ]
      [ "FV"  [FV]  ] ] <but> map <row>

  ] <col> ;

: make-left ( -- )
    [
        [ [n]   [i]   [PV]  [PMT] [FV]  ] <row>
        [ [yx]  [1/x] [%T]  [d%]  [%]   ] <row>
        [ [R/S] [SST] [R|]  [x-y] [CLx] ] <row>
        [ [ON]  [f]   [g]   [STO] [RCL] ] <row>
    ] <col> ;

: make-mid ( -- )
    [ 
        [ [CHS]   ] <row>
        [ [EEX]   ] <row>
        [ [ENTER] ] <row>
        [ [ENTER] ] <row>
        ! extend ENTER to two rows
    ] <col> ;    

: make-right ( -- )
    [
        [ "7" [#] "8" [#] "9" [#]  [÷] ] <row>
        [ "4" [#] "5" [#] "6" [#]  [x] ] <row>
        [ "1" [#] "2" [#] "3" [#]  [-] ] <row>
        [ "0" [#]     [.]     [E+] [+] ] <row>


    ] <col> ;


: calc-ui ( -- )
    [
        <display>
        make-left make-mid make-right
    ] <row> { 10 10 } <border> "Calculator" open-window ;
    
: run-calc-ui ( -- ) [ calc-ui ] with-ui ;

MAIN: run-calc-ui        

